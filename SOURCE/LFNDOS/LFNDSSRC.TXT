LFNDOS Source Code
==================

This archive contains the source code to the LFNDOS program.

It compiles with Borland Turbo C++ 3.0. Just add the two CPP files and
the OBJ file to a project, set Large memory model, and compile.
And before you ask: no, it will NOT compile with MS Visual C++ version 2 or
later, because that can't make DOS programs. You can get Turbo C++ 2.0 free
from Borland at http://community.borland.com/museum, and I think LFNDOS should
compile with that.

There is ABSOLUTELY NO WARRANTY on the included software.
The source code comes under the GNU Public License; see the file COPYING
included for information on what you can and can't do with the source.

Included in this archive is my messy unmodified LFNDOS v1.06 source code
(these are the files in the root of the archive).
Inside is also a file called lfndsnew.zip, which contains a modified
version (v1.07 beta), which has been formatted nicely and also unicode
functions added by Jiang Hong. Which version you want to use is up to you,
but I can only answer questions about my original v1.06 source, because
changes have been made to the other version, which I may not be aware of.

Chris Jones
http://members.xoom.com/dosuser/
