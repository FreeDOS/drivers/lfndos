# LFNDOS

Provides the Windows 95 long filename API to DOS programs. Any DOS program which can use long filenames, for example DOS 7 Command.com, edit.com and all DJGPP programs, can load and save using them with LFNDOS. Designed for Win95 users using DOS-mode, although it works under old DOS versions too.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## LFNDOS.LSM

<table>
<tr><td>title</td><td>LFNDOS</td></tr>
<tr><td>version</td><td>1.06a</td></tr>
<tr><td>entered&nbsp;date</td><td>2015-02-18</td></tr>
<tr><td>description</td><td>Provides  long filename support</td></tr>
<tr><td>keywords</td><td>long file name, lfn</td></tr>
<tr><td>author</td><td>Chris Jones dosuser _AT_ hotmail.com</td></tr>
<tr><td>platforms</td><td>DOS, FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
<tr><td>summary</td><td>Provides the Windows 95 long filename API to DOS programs. Any DOS program which can use long filenames, for example DOS 7 Command.com, edit.com and all DJGPP programs, can load and save using them with LFNDOS. Designed for Win95 users using DOS-mode, although it works under old DOS versions too.</td></tr>
</table>
